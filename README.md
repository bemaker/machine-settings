# Machine stuff

technical notes needed for various things to manage the various machines used in the fablabmobile.brussels project as well as the beMaker project.

We use [grbl1.1](https://github.com/gnea/grbl) for both the [eleksmaker cnc](http://eleksmill.eleksmaker.com/) machines and the [eleksmaker laser](http://elekslaser.eleksmaker.com/) cutters.

for example this repo hosts the settings we use for grbl: [grbl1-1LaserConf.txt](grbl1-1LaserConf.txt)
